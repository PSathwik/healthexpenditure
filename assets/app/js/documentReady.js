'use strict';

$(document).ready(function(){
  var source = $("#entry-template").html();
  var template = Handlebars.compile(source);
  var globalurl = "http://192.168.1.108:8080/healthapi";

  var globalfunction = function(data,dropdownid,key,value) {
  	var formateddata = formatData(data,key,value);
    var newdata = {options:formateddata}
    var html = template(newdata);
    $(dropdownid).html(html);
  };

  var formatData = function (data,key,value) {
  	var array = [];
  	for(var i = 0; i<data.length; i++)
  	{
  		array.push({ 
  			'code' : data[i][key],
  			'value' : data[i][value]
  		});
  	}
  	return array;
  };

  var isRegional = function() {
  	return $('input:radio[name=view-by]:checked').val() === 'Regional';
  };

  var getType = function(){
  	return $("#type-select").val();
  };

  var getLocal = function(){
  	return $("#comparison-select").val();
  };

  var getUrlWithType = function() {
  	var type = getType();
  	return "/data?type=" + type;
  };

  var ajaxfunc=function(endpoint,resultCB,dropdownid,key,value){
  	console.log(globalurl+endpoint);
  	var setting={
  		async:false,
  		url:globalurl+endpoint,
  		crossDomain: true,
  		type: 'GET',
  		dataType: 'json',
  		success:function(data){
  			resultCB(data,dropdownid,key,value);
  		},
  		error: function (err) {
		      console.log(err);
		  }
  	    };
  	    return setting;
  };

  var getLocalDataSettings = function() {
  	var local = getLocal();
  	var url = getUrlWithType();
  	if (isRegional()) {
      url += "&region=";
  	} else {
      url += "&state=";
    }
    url += local;
    return ajaxfunc(url, chartsModule.setLHE);
  };

  $.ajax(ajaxfunc("/", globalfunction, "#type-select", "Code", "Item"));
  $.ajax(ajaxfunc("/regions", globalfunction, "#comparison-select", "Region_Name", "Region_Name"));

  $.ajax(ajaxfunc(getUrlWithType(),chartsModule.setNHE));
  $.ajax(getLocalDataSettings());

  $('input:radio[name=view-by]').on('change',function() {
        if (this.value == 'Regional') {
             $.ajax(ajaxfunc("/regions",globalfunction,"#comparison-select","Region_Name","Region_Name"));
        }
        else if (this.value == 'State') {
             $.ajax(ajaxfunc("/states",globalfunction,"#comparison-select","State_Name","State_Name"));
        }
        $.ajax(getLocalDataSettings());
    });

  $("#type-select,#comparison-select").on('change',function(){
  		$.ajax(getLocalDataSettings());
  		if(this.id=="type-select"){
  			$.ajax(ajaxfunc(getUrlWithType(), chartsModule.setNHE));
  		}
  });

});
