'use strict';

var chartsModule = function(){

  var chart;
  var baseYear = 1980;
  var finalYear = 2009;
  
  var getYears = function(baseYear, lastYear) {
    var years = [];
    var loopIndex = 0;
    for(loopIndex = 0; loopIndex < lastYear-baseYear+1; loopIndex++) {
      years.push(baseYear + loopIndex);
    }
    return years;
  };

  var getChartSettings = function(years) {
    var settings = {
      chart: {renderTo: 'chartArea', type: 'line'},
      title: {text: 'Health Expenditure Comparison'},
      subtitle: {text: 'Source: www.cms.gov'},
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
      },
      tooltip: {valueSuffix: 'M$'},
      xAxis: {categories: years},
      yAxis: {title: {text: 'Expenditure(M$)'}},
      series: []
    };
    
    return settings;
  };

  var initialize = function() {
    if (chart === undefined) {
      var chartSettings = getChartSettings(getYears(baseYear, finalYear));
      chart = new Highcharts.Chart(chartSettings);
    }
  };
  
  var getDataArray = function(data) {
    var dataArray = [];
    var years = getYears(baseYear, finalYear);
    var loopIndex = 0;
    for(loopIndex = 0; loopIndex < years.length; loopIndex++) {
      var year = years[loopIndex];
      dataArray.push(data[year]);
    }
    return dataArray;
  };

  var addSeries = function(name, data){
    destroySeries(name);
    chart.addSeries({name: name, data: getDataArray(data)});
  };

  var destroySeries = function(name) {
    var loopIndex = 0;
    var series = chart.series;
    for(; loopIndex < series.length; loopIndex++) {
      if(series[loopIndex].name === name) {
        series[loopIndex].destroy();
      }
    }
  };

  return {
    setNHE: function(data) {
      initialize();
      addSeries('NHE', data);
    },
    setLHE: function(data) {
      initialize();
      addSeries('LHE', data);
    }
  }

}();

